package com.example.gradle;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class GradleApplicationTests {

	@Value("${test.env}")
	private String env;

	@Test
	void contextLoads() {
	}

	@Test
	void test1(){
		assertEquals(sum(1,1) ,2);
	}
	@Test
	void test2(){
		assertEquals(sum(1,1),2);
	}
	@Test
	void test3(){
		assertEquals(sum(2,2),4);
	}

	@Test
	void test4(){
		assertEquals(env,"dev");
	}

	private int sum(int a, int b) {
		return a + b;
	}

}
