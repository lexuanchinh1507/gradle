FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD build/libs/*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
